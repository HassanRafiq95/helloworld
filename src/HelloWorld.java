public class HelloWorld
{
    public static void main(String[] args) {
        String firstName = "Bob";
        String surname = "Simpson";
        String fullname = getFullName(firstName, surname);
        welcome(fullname);
        rangeOfNumber(67);
        HelloWorld helloWorld = new HelloWorld();
        helloWorld.catDogCount(helloWorld.animals);
        helloWorld.maxValue(helloWorld.ages);
        helloWorld.minValue(helloWorld.ages);
        System.out.println("End of Program");
    }

    // Initialise animal array
    String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};

    //    Initialise ages array
    int[] ages = {24, -5, -118, 0, 600, 20, 42, 50};


    public static void printLine()
    {
        System.out.println("####################");
    }

    public static String getFullName(String firstname, String surname)
    {
        return firstname + " " + surname;
    }

    public static void welcome(String name)
    {
        String output = "Hello Dr " + name + "!";
        printLine();
        System.out.println(output);
        printLine();
    }

    public static void rangeOfNumber( int x)
    {
        if (x < 10)
        {
            System.out.println("x is less than 10");
        }
        else if (x <= 100)
        {
            System.out.println("x is between 10 and 100");
        }

        else
        {
            System.out.println("x is greater than 100");
        }
    }


    public void catDogCount(String[] array)
    {
        int numDogs = 0;
        int numCats = 0;

        for (int i = 0; i < animals.length; i++)
        {
            if (animals[i] == "cat")
            {
                numCats++;
            }
            else if (animals[i] == "dog")
            {
                numDogs++;
            }
        }
        System.out.println("Number of cats: " + numCats);
        System.out.println("Number of dogs: " + numDogs);
    }

    public void maxValue(int[] ages)
    {
        // Loop to calculate Max Value
        int maxValue = ages[0];

        for (int i = 0; i < ages.length; i++) {
            if (ages[i] > maxValue) {
                maxValue = ages[i];
            }
        }
        System.out.println("The Max Value is: " + maxValue);
    }

    public void minValue(int[] ages)
    {
        // Loop to calculate Min Value
        int minValue = ages[0];

        for (int i = 0; i < ages.length; i++) {
            if (ages[i] < minValue) {
                minValue = ages[i];
            }
        }
        System.out.println("The Min Value is: " + minValue);
    }

}





